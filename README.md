[Think Brownstone](http://thinkbrownstone.com) is an experience design consultancy located in Philadelphia, PA. We have a fantastic team of designers and developers focused on providing beautiful, user-centered, accessible digital products.

Navigate to our development process and standards folders to read about the approach and practices that make us successful through build and delivery.

## Documentation Owners

The following lists the team members responsible for individual standards documents.

- Accessibility (Helene)
- CSS (Greg)
- Git (Dan Gautsch)
- HTML (Greg)
- JavaScript (TJ)
- Performance (Kimberly)
- Sass (Greg)
- SEO (Dan Gautsch)

- Automated Testing (TBD)

- Process (TBD)
- QA (TBD)

## Providing feedback

We'll use [Bitbucket's Issue Tracker](https://bitbucket.org/thinkbrownstone/tbi_dev-standards/issues?status=new&status=open) to submit feedback and updates to our standards documentation. 

Questions should also be posted in the Issue Tracker, which will be automatically posted to #ui-dev in the Think Brownstone Slack chat room.